from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m.goose import Optional
from st3m.ui.view import ViewManager
from ctx import Context
from .utils import (
    get_high_score,
    get_high_scores,
    set_high_score,
    set_direction_leds,
    parse_gyro_input,
    next_xy,
)
from .main_menu import draw_main_menu, think_main_menu
import leds
import random
import math
import bl00mbox


class SnakeGame(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        # TODO: gyro hysteresis
        # TODO: own samples
        # TODO: input buffer
        # TODO: shinier win lights
        # TODO: use ViewManager/BaseView for loading/main menu/game
        leds.set_brightness(69)
        self._grid_size = 10
        self._box_size = 7
        self._control_mode = 0  # 0 = petals, 1 = gyro
        self._edge_death = True
        self.reset_game(game_state=-1)

        self.blm = bl00mbox.Channel("Snak3")
        self._sample_load = self.load_samples()

        # Ignore inputs until released
        self.input._ignore_pressed()

    def reset_game(self, game_state=1):
        leds.set_all_rgb(0, 0, 0)
        leds.update()

        self._score = 0
        self._x = 0
        self._y = 0
        self._move_timer = 0
        self._direction = 2
        self._time = 0
        self._tail = []
        self._last_tail_move_x = 0
        self._last_tail_move_y = 0
        self._hit_high_score = False
        self._game_state = game_state  # -1 = loading, 0 = main menu, 1 = in game, 2 = game over, 3 = win
        self._high_scores = get_high_scores()
        self.relocate_food()

    def relocate_food(self):
        # hacky!
        self._food_x = self._x
        self._food_y = self._y
        while ((self._food_x, self._food_y) == (self._x, self._y)) or (
            (self._food_x, self._food_y) in self._tail
        ):
            self._food_x = random.randint(-self._grid_size, self._grid_size)
            self._food_y = random.randint(-self._grid_size, self._grid_size)

    def check_tail_collision(self):
        if (self._x, self._y) in self._tail[:-1]:
            self.set_game_over()

    def set_game_over(self, state=2):
        if state == 2:
            self.woof.signals.trigger.start()
        self._game_state = state
        if self._score > get_high_score(
            self._high_scores, self._grid_size, self._edge_death
        ):
            set_high_score(
                self._high_scores, self._grid_size, self._edge_death, self._score
            )

    def load_samples(self):
        yield
        self.nya = self.blm.new(bl00mbox.patches.sampler, "nya.wav")
        self.nya.signals.output = self.blm.mixer
        yield
        self.woof = self.blm.new(bl00mbox.patches.sampler, "bark.wav")
        self.woof.signals.output = self.blm.mixer
        yield

    def draw(self, ctx: Context) -> None:
        if self._game_state == -1:
            self.draw_loading(ctx)
        elif self._game_state == 0:
            draw_main_menu(
                ctx,
                self._control_mode,
                self._grid_size,
                self._high_scores,
                self._edge_death,
            )
        elif self._game_state in [1, 2, 3]:
            self.draw_game(ctx)

    def draw_loading(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.rgb(0.98, 0.28, 0.77).rectangle(-120, -120, 240, 240).fill()
        ctx.font = ctx.get_font_name(8)
        ctx.move_to(0, 0)
        ctx.rgb(0, 0, 0)
        ctx.text("loading...")

    def draw_game(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = ctx.get_font_name(8)

        # PUSH_PINK
        ctx.rgb(0.98, 0.28, 0.77).rectangle(-120, -120, 240, 240).fill()

        ctx.rgb(0, 0, 0).rectangle(
            -(self._box_size + 1) * (self._grid_size + 0.5),
            -(self._box_size + 1) * (self._grid_size + 0.5),
            ((self._box_size + 1) * 2) * (self._grid_size + 0.5),
            ((self._box_size + 1) * 2) * (self._grid_size + 0.5),
        ).fill()

        ctx.move_to(0, -100)
        ctx.text(str(self._score))

        ctx.font_size = 15
        high_score = get_high_score(
            self._high_scores, self._grid_size, self._edge_death
        )
        if not self._hit_high_score and self._score <= high_score:
            ctx.move_to(0, 95)
            ctx.text("high score:")
            ctx.move_to(0, 110)
            ctx.text(str(high_score))
        else:
            self._hit_high_score = True
            ctx.move_to(0, 95)
            ctx.text("new high score :)")

        # food
        ctx.rgb(1, 0, 0).rectangle(
            (self._box_size + 1) * int(self._food_x) - 4,
            (self._box_size + 1) * int(self._food_y) - 4,
            self._box_size,
            self._box_size,
        ).fill()

        # snake
        for snake_tail in self._tail:
            # AFE1AF
            ctx.rgb(0.68, 0.88, 0.68).rectangle(
                (self._box_size + 1) * int(snake_tail[0]) - 4,
                (self._box_size + 1) * int(snake_tail[1]) - 4,
                self._box_size,
                self._box_size,
            ).fill()
        # GO_GREEN
        ctx.rgb(0.25, 1.0, 0.13).rectangle(
            (self._box_size + 1) * self._x - 4,
            (self._box_size + 1) * self._y - 4,
            self._box_size,
            self._box_size,
        ).fill()

        if self._game_state in [2, 3]:
            ctx.save()
            if self._game_state == 2:
                ctx.rgb(1, 0, 0)
            elif self._game_state == 3:
                ctx.rgb(0, 0, 1)
            ctx.font_size = 25
            ctx.move_to(0, -25)
            ctx.text("game over!" if self._game_state == 2 else "you win!")
            ctx.font_size = 15
            ctx.move_to(0, 5)
            ctx.text(
                "press down left rocker"
                if self._game_state == 2
                else "was it worth wasting"
            )
            ctx.move_to(0, 20)
            ctx.text(
                "to restart game" if self._game_state == 2 else "your life on this?"
            )
            if self._hit_high_score:
                ctx.move_to(0, 35)
                ctx.text(
                    "new high score!"
                    if self._game_state == 2
                    else "new high score tho!"
                )
            elif self._game_state == 3:
                ctx.text("wait you did this before?")
            ctx.restore()

    def on_exit(self) -> None:
        leds.set_all_rgb(0, 0, 0)
        leds.update()

    def move_tail(self):
        # move tail along with our movement
        if self._last_tail_move_x != self._x or self._last_tail_move_y != int(self._y):
            if self._score == 0:
                return

            self._tail = self._tail[-self._score :]
            self._tail.append((self._x, self._y))
            self._last_tail_move_x = self._x
            self._last_tail_move_y = self._y

    def change_direction_if_safe(self, direction):
        if direction is None:
            return
        if (
            next_xy(direction, self._x, self._y, self._edge_death, self._grid_size)
            not in self._tail
        ):
            self._direction = direction
        else:
            set_direction_leds(direction, 1, 0, 0)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        leds.set_all_rgb(0, 0, 0)

        if self._game_state == -1:
            try:
                next(self._sample_load)
            except StopIteration:
                self._game_state = 0

        if self.input.buttons.app.left.pressed:
            self._control_mode = 0
        elif self.input.buttons.app.right.pressed:
            self._control_mode = 1

        if self.input.buttons.app.middle.pressed:
            self.reset_game()
            return

        # do nothing in main menu
        if self._game_state == 0:
            think_main_menu(self, ins)
            return

        if self._game_state == 2:
            leds.set_all_rgb(1, 0, 0)
            leds.update()
            return
        elif self._game_state == 3:
            # TODO: make this shinier
            leds.set_all_rgb(0, 1, 0)
            leds.update()
            return

        if self._control_mode == 0:
            for i in [0, 2, 5, 8]:
                if ins.captouch.petals[i].pressed:
                    self.change_direction_if_safe(i)
        else:
            y = ins.imu.acc[0]
            x = ins.imu.acc[1]
            self.change_direction_if_safe(parse_gyro_input(x, y, self._direction))

        # set the 3 LEDs in the move direction to green
        set_direction_leds(self._direction, 0, 1, 0)

        self._move_timer += delta_ms * max(min((0.5 * self._score), 15), 3)

        if self._move_timer > 1000:
            self._move_timer %= 1000
            x_y = next_xy(
                self._direction, self._x, self._y, self._edge_death, self._grid_size
            )
            self._x = x_y[0]
            self._y = x_y[1]

        if (
            int(max(math.fabs(self._x), math.fabs(self._y))) > self._grid_size
            and self._edge_death
        ):
            self.set_game_over()

        if self._x == self._food_x and self._y == self._food_y:
            self._score += 1
            self.nya.signals.trigger.start()
            if not self._tail:
                self._last_tail_move_x = self._x
                self._last_tail_move_y = self._y
            self.relocate_food()

        self.move_tail()
        self.check_tail_collision()

        if self._score >= (((self._grid_size * 2) + 1) ** 2) - 1:
            self.set_game_over(state=3)
        leds.update()


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(SnakeGame(ApplicationContext()))
