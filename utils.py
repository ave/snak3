import os
import json
import leds
import math

# TODO: use SD when available
HIGH_SCORE_FILE = "/flash/.snak3.json"
SPEED_MAP = {
    0: (0, -1),
    2: (1, 0),
    5: (0, 1),
    8: (-1, 0),
}


def get_high_score_name(grid_size, edge_death):
    return str(grid_size) + "_" + ("edge_death" if edge_death else "edge_loop")


def get_high_score(high_scores, grid_size, edge_death):
    return high_scores["high_scores"].get(get_high_score_name(grid_size, edge_death), 0)


def get_high_scores():
    if not os.path.exists(HIGH_SCORE_FILE):
        return {"high_scores": {}}

    with open(HIGH_SCORE_FILE) as f:
        return json.load(f)


def set_high_score(high_scores, grid_size, edge_death, score):
    high_scores["high_scores"][get_high_score_name(grid_size, edge_death)] = score
    with open(HIGH_SCORE_FILE, "w") as f:
        json.dump(
            high_scores,
            f,
        )


def set_direction_leds(direction, r, g, b):
    if direction == 0:
        leds.set_rgb(39, r, g, b)
    else:
        leds.set_rgb((direction * 4) - 1, r, g, b)
    leds.set_rgb(direction * 4, r, g, b)
    leds.set_rgb((direction * 4) + 1, r, g, b)


def get_gyro_angle(x, y):
    tilt = x**2 + y**2

    if tilt < 5:
        return None

    angle = (math.atan2(x, y) * (180 / math.pi))
    angle = (angle + 360) % 360

    return angle


def parse_gyro_input(x, y, current_direction):
    angle = get_gyro_angle(x, y)
    # If outside deadzone, ignore
    if not angle:
        return None

    # TODO: Hysteresis at 45 degrees

    if 150 < angle < 210:  # 0 = up
        return 0
    elif 60 < angle < 120:  # 90 = right
        return 2
    elif angle < 30 or angle > 330:  # 180 = down
        return 5
    elif 240 < angle < 300:  # 270 = left
        return 8


def next_xy(direction: int, x: int, y: int, edge_death: bool, grid_size: int):
    temp_x = x + SPEED_MAP[direction][0]
    temp_y = y + SPEED_MAP[direction][1]

    if not edge_death:
        if temp_x > grid_size:
            temp_x = -grid_size
        elif temp_x < -grid_size:
            temp_x = grid_size

        if temp_y > grid_size:
            temp_y = -grid_size
        elif temp_y < -grid_size:
            temp_y = grid_size

    return temp_x, temp_y
