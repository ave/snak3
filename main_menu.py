from ctx import Context
from .utils import get_high_score, set_direction_leds
from st3m.input import InputState
import leds


_main_menu_pressed = {}


def draw_main_menu(
    ctx: Context,
    control_mode: int,
    grid_size: int,
    high_scores: dict,
    edge_death: bool,
) -> None:
    ctx.text_align = ctx.MIDDLE
    ctx.text_baseline = ctx.MIDDLE
    ctx.font = ctx.get_font_name(8)

    # PUSH_PINK
    ctx.rgb(0.98, 0.28, 0.77).rectangle(-120, -120, 240, 240).fill()
    ctx.rgb(0, 0, 0).rectangle(-120, -80, 240, 160).fill()

    ctx.rgb(0, 0, 0)
    ctx.font_size = 15

    ctx.move_to(0, -105)
    ctx.text("^")

    ctx.move_to(0, -100)
    ctx.text("walls " + ("on" if edge_death else "off"))

    ctx.save()
    ctx.rgb(1, 1, 1)

    ctx.font_size = 35
    ctx.move_to(0, -50)
    ctx.text("Snak3")

    ctx.font_size = 15
    ctx.move_to(0, -20)
    ctx.text("press left shoulder to start")
    ctx.move_to(0, 5)
    ctx.text("control mode: " + ("gyro" if control_mode == 1 else "petals 0/2/5/8"))
    ctx.move_to(0, 20)
    ctx.text("flick left shoulder to change")

    ctx.move_to(0, 45)
    ctx.text("grid size: " + str(grid_size))
    ctx.move_to(0, 60)
    ctx.text("hit petal 8/2 to change")

    ctx.rgb(0, 0, 0)
    ctx.move_to(0, 95)
    ctx.text("high score:")
    ctx.move_to(0, 110)
    ctx.text(str(get_high_score(high_scores, grid_size, edge_death)))
    ctx.restore()


def think_main_menu(game_state, ins: InputState):
    global _main_menu_pressed
    if ins.captouch.petals[0].pressed:
        if not _main_menu_pressed.get(0, False):
            game_state._edge_death = not game_state._edge_death
        _main_menu_pressed[0] = True
    else:
        _main_menu_pressed[0] = False

    if game_state._edge_death:
        set_direction_leds(0, 0, 1, 0)
    else:
        set_direction_leds(0, 1, 0, 0)

    set_direction_leds(2, 1, 1, 0)
    set_direction_leds(8, 1, 1, 0)

    if ins.captouch.petals[2].pressed:
        if game_state._grid_size == 10:
            set_direction_leds(2, 1, 0, 0)
        elif not _main_menu_pressed.get(2, False):
            game_state._grid_size += 1
        _main_menu_pressed[2] = True
    else:
        _main_menu_pressed[2] = False

    if ins.captouch.petals[8].pressed:
        if game_state._grid_size == 1:
            set_direction_leds(8, 1, 0, 0)
        elif not _main_menu_pressed.get(8, False):
            game_state._grid_size -= 1
        _main_menu_pressed[8] = True
    else:
        _main_menu_pressed[8] = False

    leds.update()
